import locale
import re
import pytz
from bs4 import BeautifulSoup
from datetime import timedelta, datetime
import mwparserfromhell
import pywikibot
from werkzeuge import output
from werkzeuge.catch_exceptions_scheduled import catch_exceptions
from werkzeuge.update_verbleibende_bearbeitungen import update_verbleibende_bearbeitungen

locale.setlocale(category=locale.LC_ALL, locale="German")

wartezeit_inaktiv = timedelta(weeks=12)
sieben_tage = timedelta(days=7)


@catch_exceptions(cancel_on_failure=True)
def gazette(wikifam, botuser, begrenzte_bearbeitungen, nonpublic):
    # Einstieg
    global optin_vorlagen
    neue_verbleibende_bearbeitungen = begrenzte_bearbeitungen["verbleibende_bearbeitungen"]
    output.info("Abarbeitung der Gazette","Gazette")

    # Hole Opt-Ins
    optin_seite = botuser.getUserPage('Gazette')
    optin_wikitext = mwparserfromhell.parse(optin_seite.text)
    optin_letzter_abschnitt = optin_wikitext.get_sections()[-1]
    if optin_letzter_abschnitt.filter_headings()[0].title.strip() == "Opt-In":
        optin_vorlagen = optin_letzter_abschnitt.filter_templates()

        for vorlage in optin_vorlagen:
            if vorlage.name != "/Vorlage":
                optin_wikitext.remove(vorlage)
                continue
            if not vorlage.has("Benutzer"):
                optin_wikitext.remove(vorlage)  # Vorlage Falscheingebunden
                continue
            vorlage_benutzer = pywikibot.Page(wikifam, vorlage.get("Benutzer").value.strip("[]"))
            gazettenempfaenger = pywikibot.User(wikifam, vorlage.get("Benutzer").value.strip("[]").split(":")[1])
            if gazettenempfaenger.last_edit[2] + wartezeit_inaktiv < datetime.utcnow():
                optin_wikitext.remove(vorlage)  # Benutzer ist inaktiv
                vorlage_benutzer.getUserTalkPage().save(summary="Bot: Benachrichtigung über Austragung aus der Gazette",
                                                        appendtext="\n\n== Benachrichtigung über Austragung aus der Gazette ==\n"
                                                                   f"Hallo {vorlage_benutzer.username},\n\n"
                                                                   f"weil du eine weile keine Bearbeitungen getätigt "
                                                                   f"hast, habe ich dein Abo beendet.\n\n"
                                                                   f"Wenn du die Gazette wieder empfangen möchtest, "
                                                                   f"kannst du dich einfach unter "
                                                                   f"{optin_seite.title(as_link=True, with_ns=False)} "
                                                                   f"wieder eintragen.\n\n"
                                                                   f"Viele Grüße ~~~~")
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1
                continue

            if vorlage_benutzer.isTalkPage():  # Benutzer hat link auf Disk gesetzt, Benutzerseite wird erwartet
                vorlage.add("Benutzer", gazettenempfaenger.title(as_link=True, allow_interwiki=False))
            if vorlage.has("Neuheiten"):
                if "ja" not in vorlage.get("Neuheiten").value:
                    vorlage.remove("Neuheiten")
            if vorlage.has("Kurier"):
                if "ja" not in vorlage.get("Kurier").value:
                    vorlage.remove("Kurier")
            if vorlage.has("Rückblick"):
                if "ja" not in vorlage.get("Rückblick").value:
                    vorlage.remove("Rückblick")
            if vorlage.has("Seite"):
                if len(vorlage.get("Seite").value.strip()) == 0:
                    vorlage.remove("Seite")
            if vorlage.has("Abschnitt"):
                if len(vorlage.get("Abschnitt").value.strip()) == 0:
                    vorlage.remove("Abschnitt")
            if vorlage.has("neuerAbschnitt"):
                if "ja" not in vorlage.get("neuerAbschnitt").value:
                    vorlage.remove("neuerAbschnitt")
    else:
        update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
        return

    try:
        optin_wikitext.replace("\n\n", "\n")
    except ValueError:
        pass

    if optin_wikitext != optin_seite.text:
        optin_seite.text = str(optin_wikitext)
        optin_seite.save(summary="Bot: Aktualisiere Opt-In für Gazette")
        if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
            neue_verbleibende_bearbeitungen -= 1

    # Vorlage: Beteiligen
    beteiligen = pywikibot.Page(wikifam, "Vorlage:Beteiligen")
    beteiligen_eintraege = re.sub("<!--(.|\s|\n)*?-->", "", beteiligen.text, re.DOTALL).split("{|")[1].split("|}")[
        0].split("|-")
    beteiligen_eintraege.pop(0)
    beteiligen_nachricht = "'''Beteiligen:'''<br />\n"
    for zeile in beteiligen_eintraege:
        zeile = zeile.strip().strip("| ").split("| ")
        if len(zeile) <= 1:
            continue
        try:
            wikilink = mwparserfromhell.parse(zeile[0]).filter_wikilinks()[0]
            thema = wikilink.text
            if not thema:
                thema = wikilink.title.split(":")[1]
        except IndexError:
            thema = zeile[0].strip("'''\n:")
        ziele = mwparserfromhell.parse(zeile[1].replace("* ", "")).filter_templates()[0].get(
            "Liste").value.strip().split("\n")
        for ziel in ziele:
            ziele.remove(ziel)
            ziele.append(ziel.strip())
        beteiligen_nachricht += f"{thema}: {', '.join(ziele)}<br />"

    # Kurier
    kurier = pywikibot.Page(wikifam, "Kurier", ns=4)
    soup = BeautifulSoup(kurier.text, 'html.parser')
    spalten = {
        "links": soup.find_all(id='Linke_Spalte'),
        "rechts": soup.find_all(id='Rechte_Spalte'),
    }
    kurier_nachricht = "'''Kurier:'''<br />\n"
    for seite, inhalt in spalten.items():
        wikitext = mwparserfromhell.parse(inhalt[0].text)
        levels = [2, ] if seite == "links" else [3, ]
        abschnitte = wikitext.get_sections(include_headings=True, include_lead=False, levels=levels)
        nachrichten_links = ""
        nachrichten_rechts = ""
        topics_liste = []
        for abschnitt in abschnitte:
            ueberschrift = abschnitt.filter_headings()[0].title.strip()
            wikilinks_in_ueberschrift = mwparserfromhell.parse(ueberschrift).filter_wikilinks()
            if len(wikilinks_in_ueberschrift) > 0:
                for wikilink_in_ueberschrift in wikilinks_in_ueberschrift:
                    ueberschrift = re.sub("\[\[.*]]", str(wikilink_in_ueberschrift.title), ueberschrift, 1)
            ueberschrift_link = ueberschrift.replace("'", "")

            datum_str = re.findall(" \d{1,2}\.\d{1,2}\.?| \d{1,2}\. \w*", str(abschnitt))[-1].strip(" .") + "." + str(
                datetime.now().year)
            try:
                datum = datetime.strptime(datum_str, "%d.%m.%Y")
            except:
                try:
                    datum = datetime.strptime(datum_str, "%d. %B.%Y")
                except:
                    datum = datetime.strptime(datum_str, "%d. %b.%Y")
            if datum + sieben_tage < datum.utcnow():
                continue

            topics_liste.append(f"[[WP:K#{ueberschrift_link}|{ueberschrift}]]")
        if len(topics_liste) > 0:
            if seite == "links":
                kurier_nachricht += "''linke Spalte:'' " + ", ".join(topics_liste) + "<br />\n"
            else:
                kurier_nachricht += "''rechte Spalte:'' " + ", ".join(topics_liste) + "<br />\n"

    # Rückblick
    rueckblick = pywikibot.Page(wikifam, "Vorlage:Rückblick")
    rueckblick_nachricht = ""
    soup = BeautifulSoup(rueckblick.text, 'html.parser')
    rueckblick_zeilen = soup.find_all("onlyinclude")[0]
    rueckblick_zeilen = re.sub("<!--(.|\s|\n)*?-->", "", rueckblick_zeilen.text, re.DOTALL).strip()
    if len(rueckblick_zeilen) > 0:
        rueckblick_nachricht = f"'''Rückblick:'''<br />\n{{|\n{rueckblick_zeilen}\n|}}"

    # Projektneuheiten
    projektneuheiten = pywikibot.Page(wikifam, "Projektneuheiten", ns=4)
    projektneuheiten_wikitext = mwparserfromhell.parse(projektneuheiten.text)
    projektneuheiten_abschnitte = projektneuheiten_wikitext.get_sections(levels=[3])
    neuheiten_nachricht = ""
    fuer_programmierer = []
    fuer_jedermann = []
    sonstiges = []
    for abschnitt in projektneuheiten_abschnitte:
        ueberschrift = abschnitt.filter_headings()
        try:
            datum = datetime.strptime(ueberschrift[0].title.strip(), "%d. %B %Y")
        except:
            continue
        if datum + sieben_tage < datetime.utcnow():
            continue
        unterabschnitte = abschnitt.get_sections(levels=[4])
        for unterabschnitt in unterabschnitte:
            unterabschnitt_ueberschrift = unterabschnitt.filter_headings()[0].title.strip()
            unterabschnitt_teile = unterabschnitt.splitlines()
            unterabschnitt_teile.pop(0)
            if "für jedermann" in unterabschnitt_ueberschrift.lower():
                fuer_jedermann.extend(unterabschnitt_teile)
            elif "für programmierer" in unterabschnitt_ueberschrift.lower():
                fuer_programmierer.extend(unterabschnitt_teile)
            abschnitt.remove(unterabschnitt)
        sonstiges_test = abschnitt.splitlines()
        sonstiges_test.pop(0)
        if len(sonstiges_test) > 0:
            sonstiges.extend(sonstiges_test)
    if len(fuer_programmierer + fuer_jedermann + sonstiges) > 0:
        neuheiten_nachricht = "'''Projektneuheiten:'''<br />\n"
        if len(fuer_jedermann) > 0:
            neuheiten_nachricht += "''Für Jedermann:''\n" + "\n".join(fuer_jedermann)
        if len(fuer_programmierer) > 0:
            neuheiten_nachricht += "''Für Programmierer:''\n" + "\n".join(fuer_programmierer)
        if len(sonstiges) > 0:
            neuheiten_nachricht += "''Sonstiges:''\n" + "\n".join(sonstiges)

    optin_wikitext = mwparserfromhell.parse(optin_seite.text)
    optin_letzter_abschnitt = optin_wikitext.get_sections()[-1]
    optin_vorlagen = optin_letzter_abschnitt.filter_templates()
    jetzt = datetime.now(pytz.timezone('Europe/Berlin'))
    for vorlage in optin_vorlagen:
        empfaenger_name = vorlage.get("Benutzer").value.filter_wikilinks()[0].title
        # if str(empfaenger_name) not in ["Benutzer:Janui", "Benutzer:WosretBot"]:
        #     continue
        empfaenger = pywikibot.User(wikifam, empfaenger_name)
        if empfaenger.username.lower() in nonpublic["no_bnr"]["gazette"]:
            print(f"{empfaenger.username} wünscht keine Nachrichten!")
            continue
        if vorlage.has("Seite"):
            zielseite = vorlage.get("Seite").value.filter_wikilinks()[0].title
            ziel = pywikibot.Page(wikifam, zielseite)
        else:
            ziel = empfaenger.getUserTalkPage()
        link = mwparserfromhell.nodes.wikilink.Wikilink(optin_seite.title(), "Gazette")
        gazette_ueberschrift = f"\n\n== {link} – {jetzt.strftime('%W')}. Woche ==\n"
        gazette_nachricht = f"{beteiligen_nachricht}\n\n"
        if vorlage.has("Rückblick"):
            gazette_nachricht = f"{rueckblick_nachricht}\n\n{gazette_nachricht}"
        if vorlage.has("Kurier"):
            gazette_nachricht += f"{kurier_nachricht}\n"
        if vorlage.has("Neuheiten"):
            gazette_nachricht += f"{neuheiten_nachricht}\n"

        gazette_nachricht += "Viele Grüße ~~~~\n"

        zusammenfassung = f"Bot: Gazette – {jetzt.strftime('%W')}. Woche "

        try:
            if vorlage.has("Abschnitt"):
                ziel_wikitext = mwparserfromhell.parse(ziel.text)
                ziel_abschnitt = ziel_wikitext.get_sections(matches=vorlage.get("Abschnitt").value.strip())[0]
                if vorlage.has("neuerAbschnitt"):
                    gazette_ueberschrift = re.sub("==", "===", gazette_ueberschrift)
                    ziel_abschnitt.append("\n" + gazette_ueberschrift + gazette_nachricht)
                else:
                    ziel_wikitext.replace(ziel_abschnitt, f"== {vorlage.get('Abschnitt').value.strip()} ==\n{gazette_nachricht}")
                ziel.text = str(ziel_wikitext)
                ziel.save(summary=zusammenfassung, minor=False, botflag=True)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1

            elif ziel.isTalkPage() or vorlage.has("neuerAbschnitt"):
                ziel.save(summary=zusammenfassung, appendtext="\n" + gazette_ueberschrift + gazette_nachricht, minor=False, botflag=True)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1

            else:
                ziel.text = gazette_ueberschrift + gazette_nachricht
                ziel.save(summary=zusammenfassung, minor=False, botflag=True)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1

        except Exception as e:
            print(e)
    update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
    return
