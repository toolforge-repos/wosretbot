from datetime import datetime, timedelta, timezone
from dateutil import tz
import mwparserfromhell
import pywikibot
from werkzeuge import output, ueberwachung
import locale

from werkzeuge.catch_exceptions_scheduled import catch_exceptions
from werkzeuge.update_verbleibende_bearbeitungen import update_verbleibende_bearbeitungen

locale.setlocale(locale.LC_TIME, "de_DE")


@catch_exceptions(cancel_on_failure=True)
def tagesueberschriften(wikifam, begrenzte_bearbeitungen, botuser):  # Einstieg
    neue_verbleibende_bearbeitungen = begrenzte_bearbeitungen["verbleibende_bearbeitungen"]
    output.info("Setzen/Aktualisieren der Tagesüberschriften", "Überschriften")
    heute = datetime.now(tz.gettz('Europe/Berlin'))
    zieldatums_string = heute.strftime('%e. %B').strip()
    output.info(f"Zieldatum: {zieldatums_string}", "Überschriften")
    plus_zehn_tage_string = (heute + timedelta(days=10)).strftime('%e. %B').strip()
    plus_zwanzig_tage_string = (heute + timedelta(days=20)).strftime('%e. %B').strip()

    seitenliste = [
        {"seite": "Kandidaten für lesenswerte Artikel", "ns": 4, "untertitel": f"\n<small>Diese Kandidaturen laufen mindestens bis zum {plus_zehn_tage_string}</small>"},
        {"seite": "Kandidaturen von Artikeln, Listen und Portalen", "ns": 4, "untertitel": f"\n<small>Diese Kandidaturen laufen mindestens bis zum {plus_zehn_tage_string}/{plus_zwanzig_tage_string}</small>"},
        {"seite": "Fragen zur Wikipedia", "ns": 4, "untertitel": f""},
        {"seite": "Auskunft", "ns": 4, "untertitel": f""},
        {"seite": "Löschprüfung", "ns": 4, "untertitel": f""},
    ]

    for eintrag in seitenliste:
        versuch = 6
        while versuch >= 0:
            print(versuch)
            try:
                seite = pywikibot.Page(wikifam, eintrag["seite"], eintrag["ns"])
                seite_wikitext = mwparserfromhell.parse(seite.text, skip_style_tags=True)

                abschnitte = seite_wikitext.get_sections(levels=[1,])
                abschnitt = abschnitte[-1]
                ueberschriften = abschnitt.filter_headings()
                output.info(f"Seite: {seite.title(with_ns=False)}", "Überschriften")
                output.info(f"Gefundene Überschriften: {ueberschriften}", "Überschriften")
                zusammenfassung = ""
                if len(ueberschriften) == 0 or ueberschriften[0].level != 1:
                    output.info("Keine Überschriften gefunden", "Überschriften")
                    continue
                if ueberschriften[0].title.strip() in zieldatums_string:
                    output.info("Nichts zu tun", "Überschriften")
                elif len(ueberschriften) > 1:
                    output.info("Neuen Abschnitt einfügen", "Überschriften")
                    seite_wikitext.append(f"\n\n= {zieldatums_string} =")
                    if len(eintrag["untertitel"]) > 0:
                        seite_wikitext.append(eintrag["untertitel"])
                    seite_wikitext.append("\n\n\n")
                    zusammenfassung = "neuer Tagesabschnitt"
                else:
                    output.info("Vorhandener Abschnitt muss aktualisert werden", "Überschriften")
                    neue_ueberschrift = f"= {zieldatums_string} ="
                    if len(eintrag["untertitel"]) > 0:
                        neue_ueberschrift += eintrag["untertitel"]
                    neue_ueberschrift += "\n\n\n"
                    seite_wikitext.replace(abschnitt, neue_ueberschrift)
                    zusammenfassung = "ersetze Tagesabschnitt"

                if seite.text != str(seite_wikitext):
                    seite.text = str(seite_wikitext)
                    seite.save(summary=f"Bot: {zusammenfassung}", minor=True, botflag=True)
                    if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                        neue_verbleibende_bearbeitungen -= 1

                # nach jedem durchlauf prüfen, ob Abbruch erforderlich
                if ueberwachung.bedingungen_ueberwachen(neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen, botuser,
                                                        wikifam):
                    break
            except pywikibot.exceptions.EditConflictError:
                versuch -= 1
            else:
                versuch = -1
    update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
    return
