import pywikibot

from werkzeuge import output


def bedingungen_ueberwachen(verbleibende_bearbeitungen, bearbeitungen_begrenzen, botuser, wikifam):
    # Verarbeitung Notaus
    notaus_page = pywikibot.page.Page(wikifam, f"Benutzer:{botuser.title(with_ns=False)}/Notaus")
    if not notaus_page.text == "nein":
        output.info(f"{notaus_page.userName()} hat den Notaus gedrückt. Ausführung wird beendet")
        # pywikibot.logging.critical(f"{notaus_page.userName()} hat den Notaus gezogen. Ausführung wird beendet")
        return True
        # sys.exit(f"{notaus_page.userName()} hat den Notaus gezogen. Ausführung wird beendet")

    # Verarbeitung verbleibende Edits
    if verbleibende_bearbeitungen <= 0 and bearbeitungen_begrenzen:
        output.critical(
            f"Keine Bearbeitungen mehr übrig. Bitte Benutzer:{botuser}/Verbleibende_Bearbeitungen hochsetzen")
        return True

    # Nur aktiv werden, wenn ich nicht gesperrt wurde
    if botuser.is_blocked():
        output.critical(f"Ich wurde gesperrt")
        return True

    return False
