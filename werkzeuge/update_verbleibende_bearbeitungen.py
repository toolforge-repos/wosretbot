from pywikibot.scripts.generate_user_files import pywikibot
from werkzeuge import output


def update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen,begrenzte_bearbeitungen):
    # Update der verbleibenden Bearbeitungen
    verbleibende_bearbeitungen_page = pywikibot.page.Page(
        wikifam,
        f"Benutzer:{botuser.title(with_ns=False)}/Verbleibende_Bearbeitungen")
    if neue_verbleibende_bearbeitungen < begrenzte_bearbeitungen["verbleibende_bearbeitungen"] and begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
        verbleibende_bearbeitungen_page.text = neue_verbleibende_bearbeitungen
        verbleibende_bearbeitungen_page.save(summary=f"Bot: Verbleibende bearbeitungen updaten. Rest: {neue_verbleibende_bearbeitungen}")
        output.info("Verbleibende Bearbeitungen wurden geupdatet")

