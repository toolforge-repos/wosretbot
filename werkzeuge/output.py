from datetime import datetime
import pywikibot
from dateutil import tz


def output(msg, prefix=""):
    pywikibot.output(f"{datetime.now(tz.gettz('Europe/Berlin')).strftime('%d.%m.%y %H:%M:%S')}({prefix}): {msg}")


def info(msg, prefix=""):
    pywikibot.info(f"{datetime.now(tz.gettz('Europe/Berlin')).strftime('%d.%m.%y %H:%M:%S')}({prefix}): {msg}")


def critical(msg, prefix=""):
    pywikibot.critical(f"{datetime.now(tz.gettz('Europe/Berlin')).strftime('%d.%m.%y %H:%M:%S')}({prefix}): {msg}")
