from datetime import datetime
from time import sleep
import schedule
from pytz import timezone
import logging


logging.basicConfig()
schedule_logger = logging.getLogger('schedule')
schedule_logger.setLevel(level=logging.INFO)

def job():
    print(str(datetime.utcnow()) + ": I'm working...1")


def job2():
    print(str(datetime.utcnow()) + ": Job2")


def job3():
    sleep(10)
    print(str(datetime.utcnow()) + ": Job3")


def job4():
    print(str(datetime.utcnow()) + ": Job4")


schedule.every().minute.do(job)
schedule.every().day.at("14:15", "Europe/Amsterdam").do(job4)
schedule.every().day.at("14:20", timezone("Europe/Amsterdam")).do(job3)
schedule.every().day.at("14:20", "Europe/Amsterdam").do(job2)

while True:
    schedule.run_pending()
    sleep(1)

