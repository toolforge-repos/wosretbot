import re
from datetime import datetime

import pymysql
from dateutil.relativedelta import relativedelta

import mwparserfromhell
import pywikibot

from werkzeuge import ueberwachung, output
from werkzeuge.catch_exceptions_scheduled import catch_exceptions
from werkzeuge.update_verbleibende_bearbeitungen import update_verbleibende_bearbeitungen

################################################
# Relevante Vorlagen
# {{subst:ArchivMentee1}} Entlassung ohne Zeitangabe oder geschlecht
# {{subst:ArchivMentee2|acht|Mentor=NAME}} Entlassung mit Zeitangabe und Geschlecht
# {{subst:ArchivMentee3}} Entlassung wegen Erfolg
# {{subst:ArchivMentee4}} Entlassung wegen max Betreuungszeit
# {{ers:Aus Mentorenprogramm (Benachrichtigung)|Name des Mentee1|Name des Mentee2}} Info an Mentor wegen entlassung aufgrund von inaktivität
# {{ers:Aus Mentorenprogramm (Benachrichtigung_2)|Name des Mentee1|Name des Mentee2}} Info an Mentor wegen entlassung nach Max Betreuungszeit
#################################################


wartezeit_mentee_inaktiv_hinweis = relativedelta(months=+2)
wartezeit_mentee_inaktiv_zuentlassen = relativedelta(months=+5)
wartezeit_mentee_max_betreuungzeit_hinweis = relativedelta(months=+12)
wartezeit_mentee_max_betreuungzeit_zuentlassen = relativedelta(months=+15)


@catch_exceptions(cancel_on_failure=True)
def menteeaktivitaet(wikifam, begrenzte_bearbeitungen, sql_verbindung, sql_tabellen, botuser, nonpublic):
    # Einstieg
    neue_verbleibende_bearbeitungen = begrenzte_bearbeitungen["verbleibende_bearbeitungen"]
    output.info("Abarbeitung der Menteeaktivität", "Menteeaktivität")
    metoren_mentee_liste = {}

    # Hole die User die eine Info wünschen, wenn Mentee inaktiv wird
    optin_seite = mwparserfromhell.parse(pywikibot.Page(wikifam, "Mentorenprogramm/Projektorganisation/Opt-in-Liste", ns=4).text)
    optin_warnung = []
    for abschnitt in optin_seite.get_sections():
        ueberschrift = re.search("== (.*) ==", str(abschnitt))
        if ueberschrift:
            ueberschrift = ueberschrift[1]  # in 0 steht der komplette match in 1 der string der durch die klammern erfasst wurde
            if ueberschrift == "Opt-in-Liste: Benachrichtigung zur fristgerechten Entlassung":
                benutzer = re.findall("# \[\[(.*)\]\]\n", str(abschnitt))
                if benutzer:
                    for benutzer in benutzer:
                        optin_warnung.append(benutzer.split("|")[1].replace("&nbsp;", " "))

    # Hole liste aller Mentoren und lege sie ab
    mp_seite = pywikibot.page.Page(wikifam, "Mentorenprogramm", ns=4)
    mp_vorlagen = mwparserfromhell.parse(mp_seite.text).filter_templates(matches="{{Mentor")
    for vorlage in mp_vorlagen:
        mentor = vorlage.get("Mentor").value.strip("\n ")
        mentor_benutzer = pywikibot.User(wikifam, mentor)
        mentor_seite = mentor_benutzer if not mentor_benutzer.isTalkPage() else mentor_benutzer.toggleTalkPage()
        mentor_disk = mentor_benutzer if mentor_benutzer.isTalkPage() else mentor_benutzer.toggleTalkPage()

        metoren_mentee_liste[mentor] = {
            "benutzer": mentor_benutzer,
            "seite": mentor_seite,
            "disk": mentor_disk,
            "mentees": {},
        }

    # Parse liste Menees in Betreuung
    # Wikipedia:Mentorenprogramm/Projektorganisation/In_Betreuung
    mentee_in_betreuung_seite = pywikibot.Page(wikifam, "Wikipedia:Mentorenprogramm/Projektorganisation/In Betreuung")
    mentee_in_betreuung_vorlagen = mwparserfromhell.parse(mentee_in_betreuung_seite.text).filter_templates(
        matches="MP-NB")
    mentee_in_betreuung = {}
    for vorlage in mentee_in_betreuung_vorlagen:
        mentee = vorlage.get("Mentee").value.strip("\n ")
        mentee_in_betreuung[mentee] = {
            "mentor": vorlage.get("Mentor").value.strip("\n "),
            "eintritt": datetime.strptime(vorlage.get("Eintritt").value.strip(), '%Y-%m-%d'),
        }

    # Parse Mentees
    mentee_kategorie = pywikibot.Category(wikifam, title="Kategorie:Benutzer:Mentee")
    for mentee in mentee_kategorie.articles():
        # Mentee Seiten abrufen
        mentee_benutzer = pywikibot.User(wikifam, mentee.title(with_ns=False))
        mentee_seite = mentee_benutzer if not mentee_benutzer.isTalkPage() else mentee_benutzer.toggleTalkPage()
        mentee_disk = mentee_benutzer if mentee_benutzer.isTalkPage() else mentee_benutzer.toggleTalkPage()

        # dazugehörigen Mentor suchen
        # ausgangsstring nach filter_templates().name Benutzer:Xyz_abc/Vorlage Mentor -> xyz abc
        try:
            hinweis_mentorat_vorlage = mwparserfromhell.parse(mentee_seite.text).filter_templates(matches="Hinweis Mentorat")[0]
            mentor_name = hinweis_mentorat_vorlage.get("Mentor").value.strip().replace("_", " ")
            print(mentor_name)
            # mentor_name = \
            #     mwparserfromhell.parse(mentee_seite.text).filter_templates(matches="/Vorlage Mentor")[0].name.split(
            #         "/")[
            #         0].split(":")[1].replace("_", " ")

            if mentor_name not in metoren_mentee_liste:  # zur sicherheit
                metoren_mentee_liste[mentor_name] = {"mentees": {}}

            #  Daten in Dict hinterlegen
            metoren_mentee_liste[mentor_name]["mentees"][mentee.username.strip("\n ")] = {
                "letzte_bearbeitung": mentee_benutzer.last_edit[2],
                "benutzer": mentee_benutzer,
                "seite": mentee_seite,
                "disk": mentee_disk,
                "eintritt": mentee_in_betreuung.get(mentee.username, datetime.utcnow()).get("eintritt", datetime.utcnow()),
                "frueherer_mentor": "",
            }
            if mentee_in_betreuung[mentee.username]["mentor"] != mentor_name:
                metoren_mentee_liste[mentor_name]["mentees"][mentee.username]["frueherer_mentor"] = \
                mentee_in_betreuung[mentee.username]["mentor"]
        except IndexError:  # Wenn falsche Vorlage eingebunden: Ersetzen
            output.output("Vorlage scheinbar fehlerhaft eingebunden", "Menteeaktivität")
            mentee_seite_wikitext = mwparserfromhell.parse(mentee_seite.text)
            vorlagen = mentee_seite_wikitext.filter_templates()
            # Todo: umschreiben, dass sowohl, {{Mentee, Wikipedia:Mentorenprogramm/Mentorenvorlage/ als aus /vorlage_mentor abgefangen werden kann
            for vorlage in vorlagen:
                if "mentee" in vorlage.name.lower():
                    mentee_seite_wikitext.replace(vorlage, f"{{{{WP:Mentorenprogramm/Hinweis Mentorat |Mentor={vorlage.get('Mentor').value.strip()}}}}}")
                    break

            mentee_seite.text = str(mentee_seite_wikitext)
            mentee_seite.save(summary="Bot: Ersetze falsche Vorlage", minor=True, botflag=True)
            if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                neue_verbleibende_bearbeitungen -= 1

            hinweis_mentorat_vorlage = mentee_seite_wikitext.filter_templates(matches="Hinweis Mentorat")[0]
            mentor_name = hinweis_mentorat_vorlage.get("Mentor").value.split().replace("_", " ")
            # mentor_name = mentee_seite_wikitext.filter_templates(matches="/Vorlage Mentor")[0].name.split("/")[0].split(":")[1].replace("_", " ")
            if mentor_name not in metoren_mentee_liste:  # zur sicherheit
                metoren_mentee_liste[mentor_name] = {"mentees": {}}

            #  Daten in Dict hinterlegen
            metoren_mentee_liste[mentor_name]["mentees"][mentee.username] = {
                "letzte_bearbeitung": mentee_benutzer.last_edit[2],
                "benutzer": mentee_benutzer,
                "seite": mentee_seite,
                "disk": mentee_disk,
                "eintritt": mentee_in_betreuung[mentee.username]["eintritt"],
                "frueherer_mentor": "",
            }
            if mentee_in_betreuung[mentee.username]["mentor"] != mentor_name:
                metoren_mentee_liste[mentor_name]["mentees"][mentee.username]["frueherer_mentor"] = mentee_in_betreuung[mentee.username]["mentor"]

    for mentor, data in metoren_mentee_liste.items():
        mentor_benutzer = pywikibot.User(wikifam, mentor)
        output.output(mentor_benutzer.title(), "Menteeaktivität")
        if mentor_benutzer.username.lower() in nonpublic["no_bnr"]["menteeaktivitaet"]:
            output.output(f"{mentor_benutzer.username} wünscht keine Aktualisierung", "Menteeaktivität")
            continue
        # mentor_seite = mentor_benutzer if not mentor_benutzer.isTalkPage() else mentor_benutzer.toggleTalkPage()
        mentor_disk = mentor_benutzer if mentor_benutzer.isTalkPage() else mentor_benutzer.toggleTalkPage()
        mentee_warnung = {}
        mentee_inaktiv = {}
        mentee_warnung_max_betreuung = {}
        mentee_max_betreuung = {}
        for mentee, data_mentee in data["mentees"].items():
            mentee_sql_data = {}
            with sql_verbindung.cursor(pymysql.cursors.DictCursor) as cursor:
                query = f"select info_max_betreuung, info_inaktiv from {sql_tabellen['menteeaktivitaet']} where mentee=%s;"
                cursor.execute(query, mentee)
                mentee_sql_data = cursor.fetchone()
            try:
                letzte_bearbeitung = data_mentee["letzte_bearbeitung"]
                eintritt = data_mentee["eintritt"]
                # Seit 5 Monaten inaktiv. Mentee sollte entlassen werden
                if letzte_bearbeitung + wartezeit_mentee_inaktiv_zuentlassen < datetime.utcnow():
                    mentee_seite = data_mentee["seite"]
                    mentee_disk = data_mentee["disk"]
                    mentee_benutzer = data_mentee["benutzer"]
                    mentee_seite.text = re.sub("{{WP:Mentorenprogramm/Hinweis Mentorat\s*\|Mentor=.*}}\s*", "", mentee_seite.text)
                    # mentee_seite.text = re.sub("{{.*/Vorlage[_ ]Mentor}}\n*", "", mentee_seite.text)
                    mentee_seite.save(
                        summary=f"Bot: {mentee_benutzer.title(as_link=False, allow_interwiki=False)} aus Mentorenprogramm entlassen",
                        minor=False)
                    if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                        neue_verbleibende_bearbeitungen -= 1
                    mentee_nachricht = (f"\n\n{{{{subst:ArchivMentee2|fünf|Mentor={mentor}}}}}\n\n"
                                        f"Ps: Ich bin nur ein [[WP:Bots|Computerprogramm]]. Wenn du mir antworten "
                                        f"möchtest, oder Fragen zu dieser Mitteilung hast, klicke bitte "
                                        f"[[BD:{botuser.username}|hier]] und hinterlass eine Nachricht.\n")
                    mentee_disk.save(summary=f"Bot: Hinweis über Entlassung aus dem Mentorenprogramm",
                                     appendtext=mentee_nachricht,
                                     minor=False, botflag=False)
                    if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                        neue_verbleibende_bearbeitungen -= 1
                    mentee_inaktiv[mentee] = {
                        "benutzer": mentee_benutzer,
                        "seite": mentee_seite,
                        "disk": mentee_disk,
                    }
                # Vorgesehene maximale Betreuungszeit von 15 Monaten überschritten
                elif eintritt + wartezeit_mentee_max_betreuungzeit_zuentlassen < datetime.utcnow():
                    mentee_seite = data_mentee["seite"]
                    mentee_disk = data_mentee["disk"]
                    mentee_benutzer = data_mentee["benutzer"]
                    # Entlassung nach 15 Monaten Betreuung deaktiviert
                    # mentee_seite.text = re.sub("{{WP:Mentorenprogramm/Hinweis Mentorat |Mentor=.*}}\n*", "", mentee_seite.text)
                    # # mentee_seite.text = re.sub("{{.*/Vorlage[_ ]Mentor}}\n*", "", mentee_seite.text)
                    # mentee_seite.save(
                    #     summary=f"Bot: {mentee_benutzer.title(as_link=True, allow_interwiki=False)} aus Mentorenprogramm entlassen",
                    #     minor=False)
                    # if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    #     neue_verbleibende_bearbeitungen -= 1
                    # mentee_disk.save(summary=f"Bot: Hinweis über Entlassung aus dem Mentorenprogramm",
                    #                  appendtext=f"\n\n{{{{subst:ArchivMentee4}}}}",
                    #                  minor=False, botflag=False)
                    # if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    #     neue_verbleibende_bearbeitungen -= 1
                    mentee_max_betreuung[mentee] = {
                        "benutzer": mentee_benutzer,
                        "seite": mentee_seite,
                        "disk": mentee_disk,
                    }
                # Seit 2 Monaten inaktiv. Hinweis an Mentor
                elif letzte_bearbeitung + wartezeit_mentee_inaktiv_hinweis < datetime.utcnow():
                    if mentee_sql_data and mentee_sql_data["info_inaktiv"]:
                        pass
                    else:
                        mentee_warnung[mentee] = {
                            "benutzer": data_mentee["benutzer"],
                            "seite": data_mentee["seite"],
                            "disk": data_mentee["disk"],
                        }
                # Seit 12 Monaten im Programm. Hinweis an Mentor
                elif eintritt + wartezeit_mentee_max_betreuungzeit_hinweis < datetime.utcnow():
                    if mentee_sql_data and mentee_sql_data["info_inaktiv"]:
                        pass
                    else:
                        mentee_warnung_max_betreuung[mentee] = {
                            "benutzer": data_mentee["benutzer"],
                            "seite": data_mentee["seite"],
                            "disk": data_mentee["disk"],
                        }
                else:
                    continue
            except KeyError:
                # dürfte nicht mehr auftreten
                output.output("Mentee steht noch in Liste, aber wird scheinbar von MentorIn nicht mehr betreut?", "Menteeaktivität")
                pass

        # Nur abarbeiten, wenn es auch was zu tun gibt
        if len(mentee_max_betreuung) + len(mentee_inaktiv) + len(mentee_warnung) >= 1:
            output.output("############################################################", "Menteeaktivität")
            output.output(f"Mentor: {mentor}", "Menteeaktivität")
            output.output(f"Bereits seit 15 Monaten in Betreuung: {len(mentee_max_betreuung)}", "Menteeaktivität")
            output.output(", ".join(mentee_max_betreuung.keys()), "Menteeaktivität")
            output.output(f"Bereits seit 12 Monaten in Betreuung: {len(mentee_warnung_max_betreuung)}", "Menteeaktivität")
            output.output(", ".join(mentee_warnung_max_betreuung.keys()), "Menteeaktivität")
            output.output(f"Bereits seit 5 Monaten inaktiv:{len(mentee_inaktiv)}", "Menteeaktivität")
            output.output(", ".join(mentee_inaktiv.keys()), "Menteeaktivität")
            output.output(f"Länger als 2Monate inaktiv: {len(mentee_warnung)}", "Menteeaktivität")
            output.output(", ".join(mentee_warnung.keys()), "Menteeaktivität")
            output.output("____________________________________________________________", "Menteeaktivität")
            mentor_nachricht = ""
            zusammenfassung = ""
            if len(mentee_inaktiv) >= 1:
                zusammenfassung += " Entlassung(en) aus Mentorenprogramm;"
                mentor_nachricht += "\n\n{{ers:Aus Mentorenprogramm (Benachrichtigung)"
                for _, data_mentee in mentee_inaktiv.items():
                    mentor_nachricht += f"|{data_mentee['benutzer'].username}"
                mentor_nachricht += "}}"
            if len(mentee_inaktiv) > 2:
                mentor_nachricht += "\n\nPS: es handelt sich dabei um folgende Mentees:"
                for _, data_mentee in mentee_inaktiv.items():
                    mentor_nachricht += f"\n* {data_mentee['benutzer'].title(as_link=True, allow_interwiki=False)}"
                mentor_nachricht += "\n\nViele Grüße ~~~~"
            # entlassung bei > 15 Monaten Betreuung deaktiviert
            # if len(mentee_max_betreuung) >= 1:
            #     mentor_nachricht += f"\n\n{{{{ers:Aus Mentorenprogramm (Benachrichtigung 2)|{'|'.join(mentee_max_betreuung.keys())}}}}}"
            # if len(mentee_max_betreuung) > 2: # muss geupdatet werden bei verwendung
            #     mentor_nachricht += "\n\nPS: es handelt sich dabei um folgende Mentees:\n# " + '\n# '.join(mentee_max_betreuung.keys()) + "\n\nViele Grüße ~~~~")
            if mentor in optin_warnung:
                if len(mentee_warnung) >= 1:
                    zusammenfassung += " Benachrichtigung über inaktive Mentees;"
                    mentor_nachricht += (f"\n\n== Benachrichtigung über inaktive Mentees am {datetime.utcnow().strftime('%d.%m.%Y')} ==\n"
                                         "Die folgenden Mentees sind bereits seit mehr als 2 Monaten inaktiv:")
                    for _, data_mentee in mentee_warnung.items():
                        mentor_nachricht += f"\n* {data_mentee['benutzer'].title(as_link=True, allow_interwiki=False)}"
                    mentor_nachricht += "\n\nViele Grüße ~~~~"
                if len(mentee_warnung_max_betreuung) >= 1:
                    zusammenfassung += " Betreuungszeit >= 12 Monate;"
                    mentor_nachricht += (f"\n\n== Benachrichtigung über lange Betreuungszeit {datetime.utcnow().strftime('%d.%m.%Y')} ==\n"
                                         "Die folgenden Mentees sind bereits seit mehr als 12 Monaten bei dir in Betreuung:")
                    for _, data_mentee in mentee_warnung_max_betreuung.items():
                        mentor_nachricht += f"\n* {data_mentee['benutzer'].title(as_link=True, allow_interwiki=False)}"
                    mentor_nachricht += "\n\nViele Grüße ~~~~"
            output.output(mentor_nachricht, "Menteeaktivität")
            if len(mentor_nachricht) > 0:
                mentor_disk.save(summary=f"Bot: Info über: {zusammenfassung}", appendtext=mentor_nachricht,
                                 minor=False, botflag=False)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1
            with sql_verbindung.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
                query = f"DELETE FROM {sql_tabellen['menteeaktivitaet']} WHERE mentee=%s;"
                cursor.executemany(query, mentee_inaktiv.keys())
                sql_verbindung.commit()
                query = f"INSERT INTO {sql_tabellen['menteeaktivitaet']} (mentee,info_max_betreuung,info_inaktiv) VALUES (%s,1,0) ON DUPLICATE KEY UPDATE info_max_betreuung=1;"
                cursor.executemany(query, mentee_warnung_max_betreuung.keys())
                sql_verbindung.commit()
                query = f"INSERT INTO {sql_tabellen['menteeaktivitaet']} (mentee,info_max_betreuung,info_inaktiv) VALUES (%s,0,1) ON DUPLICATE KEY UPDATE info_inaktiv=1;"
                cursor.executemany(query, mentee_warnung.keys())
                sql_verbindung.commit()

        if ueberwachung.bedingungen_ueberwachen(neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen, botuser, wikifam):
            break

    update_verbleibende_bearbeitungen(wikifam,botuser,neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
    return
