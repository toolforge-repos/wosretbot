import datetime
import mwparserfromhell
import pymysql
import pywikibot

from werkzeuge import output, ueberwachung
from werkzeuge.catch_exceptions_scheduled import catch_exceptions
from werkzeuge.update_verbleibende_bearbeitungen import update_verbleibende_bearbeitungen

wartezeit_bis_mentor_inaktiv = datetime.timedelta(days=7)


# ToDO: Mentor inaktiv info? {{subst:MentorInaktiv}}

@catch_exceptions(cancel_on_failure=True)
def mentorenaktivitaet(wikifam, begrenzte_bearbeitungen, sql_verbindung, sql_tabellen, botuser, nonpublic):
    neue_verbleibende_bearbeitungen = begrenzte_bearbeitungen["verbleibende_bearbeitungen"]
    output.info("Abarbeitung der Mentorenaktivität", "Mentorenaktivität")
    mp_seite = pywikibot.page.Page(wikifam, "Mentorenprogramm", ns=4)
    com_seite = pywikibot.page.Page(wikifam, "Mentorenprogramm/Co-Übersicht", ns=4)
    wikicode = mwparserfromhell.parse(mp_seite.text)
    mp_vorlagen = wikicode.filter_templates(matches="{{Mentor")

    mentoren_wieder_aktiv = []
    mentoren_jetzt_inaktiv = []
    zusammenfassung_mentorenseite = ""
    co_metoren_uebersichtsseite = (
        '<!-- Diese Seite wird automatisch erstellt. Änderungswünsche bitte beim Bot-Betreiber melden -->\n'
        'Ein in kursiver Schrift stehender Name bedeutet, dass dieser Mentor auf der Seite '
        '[[WP:Mentorenprogramm]] auf Pause gesetzt ist: <br />(per Bot) wegen momentaner '
        'Inaktivität (von mindestens sieben Tagen), Urlaub, sonstiger Abwesenheit oder weil er '
        'zur Zeit keine [[:Kategorie:Benutzer:Wunschmentor gesucht|Wunschmentorengesuche]] '
        'empfangen kann oder möchte.\n\n'
        '{{Tabellenstile}}\n'
        '{| class="wikitable sortable tabelle-zaehler"\n'
        '! Mentor\n'
        '! Co-Mentor1\n'
        '! Co-Mentor2\n'
        '! Co-Mentor3\n'
        '! Co-Mentor4\n')

    for vorlage in mp_vorlagen:
        # Pausen feststellen
        mentor_benutzer = pywikibot.User(wikifam, vorlage.get("Mentor").value)
        mentor_disk = mentor_benutzer if mentor_benutzer.isTalkPage() else mentor_benutzer.toggleTalkPage()
        ist_inaktiv = True if mentor_benutzer.last_edit[
                                  2] < datetime.datetime.utcnow() - wartezeit_bis_mentor_inaktiv else False
        pause_gesetzt = True if "ja" in vorlage.get("Pause").value.lower() else False
        # mentor_mp_vorlagen_seite = mentor_benutzer.getUserPage("Vorlage Mentor")  # Umgezogen
        mentor_mp_vorlagen_seite = pywikibot.Page(wikifam, f"Wikipedia:Mentorenprogramm/Mentorenvorlage/{mentor_benutzer.username}")
        mentor_mp_vorlagen_seite_wikitext = mwparserfromhell.parse(mentor_mp_vorlagen_seite.text)
        # mentor_mp_vorlage = mentor_mp_vorlagen_seite_wikitext.filter_templates(matches="{{Mentee")[0]
        # mentor_benutzerseite_vorlagen = mwparserfromhell.parse(mentor_benutzer.getUserPage().text).filter_templates()
        will_inaktiv_sein = False
        war_inaktiv = False
        # benutzer_inaktiv_baustein_erstmalig = False
        with (sql_verbindung.cursor(pymysql.cursors.DictCursor) as cursor):
            querry = f"select * from {sql_tabellen['mentorenaktivitaet']} where mentor=%s"
            cursor.execute(querry, mentor_benutzer.username.lower())
            ergebnis = cursor.fetchone()
            if ergebnis and ergebnis["will_pause"]:
                will_inaktiv_sein = True
            if ergebnis and ergebnis["ist_inaktiv"]:
                war_inaktiv = True
        # for benutzer_vorlage in mentor_benutzer_mp_vorlage:
        #     if "Benutzer inaktiv" in benutzer_vorlage.name:
        #         will_inaktiv_sein = True
        #         break
        # for benutzer_vorlage in mentor_benutzerseite_vorlagen:
        #     if "Benutzer inaktiv" in benutzer_vorlage.name and not will_inaktiv_sein:
        #         will_inaktiv_sein = True
        #         benutzer_inaktiv_baustein_erstmalig = True
        #         break
        # Pause wurde manuell gesetzt
        if pause_gesetzt and not war_inaktiv:
            will_inaktiv_sein = True
        # Pause wurde manuell entfernt
        elif not pause_gesetzt and will_inaktiv_sein:
            will_inaktiv_sein = False

        mentor_nachricht = ""
        # Prüfen ob Pause entfernt werden soll
        if pause_gesetzt and not ist_inaktiv and not will_inaktiv_sein:
            mentoren_wieder_aktiv.append(mentor_benutzer)
            vorlage.remove("Pause", keep_field=True)
            mentor_nachricht = ("\n\n== Deine Pause im Mentorenprogramm wurde beendet ==\n\n"
                                "Da du wieder aktiv bist, habe ich deine Pause im Mentorenprogramm entfernt. Falls du "
                                "weiterhin auf Pause stehen möchtest, dann trage sie bitte wieder ein (sie wird dann "
                                "auch nicht wieder herausgenommen). -- ~~~~\n")
            summary = "Bot: Deine Pause im Mentorenprogramm wurde beendet"
        # Prüfen ob Pause gesetzt werden soll
        elif not pause_gesetzt and ist_inaktiv:
            mentoren_jetzt_inaktiv.append(mentor_benutzer)
            vorlage.add("Pause", "ja")
            mentor_nachricht = ("\n\n== Du wurdest im Mentorenprogramm auf Pause gesetzt ==\n\n"
                                "Da du länger als 7 Tage keine Bearbeitungen getätigt hast, wurdest du automatisch im "
                                "Mentorenprogramm auf Pause gesetzt. Wenn du wieder aktiv bist, werde ich in der "
                                "darauffolgenden Nacht die Pause wieder beenden. --~~~~\n")
            summary = "Bot: Du wurdest im Mentorenprogramm auf Pause gesetzt"

        if len(mentor_nachricht) > 0:
            print(mentor_nachricht)
            mentor_disk.save(summary=summary, appendtext=mentor_nachricht,
                             minor=False, botflag=False)
            if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                neue_verbleibende_bearbeitungen -= 1

        with sql_verbindung.cursor() as cursor:
            querry = f"INSERT INTO {sql_tabellen['mentorenaktivitaet']} (mentor, ist_inaktiv, will_pause) VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE ist_inaktiv=%s, will_pause=%s"
            cursor.execute(querry,
                           (mentor_benutzer.username, ist_inaktiv, will_inaktiv_sein, ist_inaktiv, will_inaktiv_sein,))
            sql_verbindung.commit()

    for vorlage in mp_vorlagen:
        mentor_benutzer = pywikibot.User(wikifam, vorlage.get("Mentor").value)
        # mentor_mp_vorlagen_seite = mentor_benutzer.getUserPage("Vorlage Mentor") # Umgezogen
        mentor_mp_vorlagen_seite = pywikibot.Page(wikifam, f"Wikipedia:Mentorenprogramm/Mentorenvorlage/{mentor_benutzer.username}")
        mentor_mp_vorlagen_seite_wikitext = mwparserfromhell.parse(mentor_mp_vorlagen_seite.text)
        try:
            mentor_mp_vorlage = mentor_mp_vorlagen_seite_wikitext.filter_templates(matches="{{Mentee")[0]
        except:
            continue
        # Baue Co-Mentoren Übersichtsseite
        mentor_name = vorlage.get('Mentor').value.strip()
        if "ja" in vorlage.get("Pause").value:
            mentor_link = f"''[[Benutzer:{mentor_name}|{mentor_name}]]''"
        else:
            mentor_link = f"[[Benutzer:{mentor_name}|{mentor_name}]]"
        co_mentor_list = []
        co_mentor_link_list = []
        for i in range(1, 5):
            if not vorlage.has(f'Co-Mentor{i}'):
                continue
            com = vorlage.get(f'Co-Mentor{i}').value.strip()
            if com == "":
                continue
            co_mentor_list.append(com)
            com_pause = False
            for e in mp_vorlagen:
                if e.get("Mentor").value.strip() == com:
                    if "ja" in e.get("Pause").value:
                        com_pause = True
                        break
            if com_pause:
                co_mentor_link_list.append(f"''[[Benutzer:{com}|{com}]]''")
            else:
                co_mentor_link_list.append(f"[[Benutzer:{com}|{com}]]")
        for i in range(1, 5):
            try:
                vorlage.add(f'Co-Mentor{i}', co_mentor_list[i - 1], before="Thema")
                mentor_mp_vorlage.add(f'Co-Mentor{i}', co_mentor_list[i - 1])
            except IndexError:
                if i == 1:
                    vorlage.add(f'Co-Mentor{i}', "", before="Thema")
                elif vorlage.has(f'Co-Mentor{i}'):
                    vorlage.remove(f'Co-Mentor{i}')
                if i == 1:
                    mentor_mp_vorlage.add(f'Co-Mentor{i}', "")
                if mentor_mp_vorlage.has(f'Co-Mentor{i}'):
                    mentor_mp_vorlage.remove(f'Co-Mentor{i}')
        while len(co_mentor_link_list) <= 4:
            co_mentor_link_list.append("")  # Liste auffüllen

        if mentor_mp_vorlagen_seite.text != str(mentor_mp_vorlagen_seite_wikitext):
            if mentor_name.lower() in nonpublic["no_bnr"]["mentoraktivitaet"]:
                output.output(f"{mentor_name} wünscht keine Aktualisierung von Mentorenvorlage", "Mentorenaktivität")
            elif mentor_mp_vorlagen_seite.has_permission():
                zusammenfassung_mentorenseite = "aktualisierung/sortierung Co-Mentoren"
                mentor_mp_vorlagen_seite.text = str(mentor_mp_vorlagen_seite_wikitext)
                mentor_mp_vorlagen_seite.save(summary="Bot: aktualisiere/sortiere Co-Mentoren; entferne leere Co-Mentoren-Parameter", minor=False,
                                              botflag=False)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1
            else:
                output.output(f"{mentor_name} hat seine Mentorenvorlage geschützt!", "Mentorenaktivität")

        co_metoren_uebersichtsseite += "|-\n"
        co_metoren_uebersichtsseite += f"| {mentor_link} || {co_mentor_link_list[0]} || {co_mentor_link_list[1]} || {co_mentor_link_list[2]} || {co_mentor_link_list[3]}\n"

        # nach jedem durchlauf prüfen ob Abbruch erforderlich
        if ueberwachung.bedingungen_ueberwachen(neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen, botuser,
                                                wikifam):
            return

    co_metoren_uebersichtsseite += "|}\n\n[[Kategorie:Wikipedia:Mentorenprogramm|Co-Übersicht]]"

    if len(mentoren_jetzt_inaktiv) > 0:
        zusammenfassung_mentorenseite = f"setze {', '.join([item.title(as_link=True, allow_interwiki=False) for item in mentoren_jetzt_inaktiv])} auf Pause; " + zusammenfassung_mentorenseite
        pass

    if len(mentoren_wieder_aktiv) > 0:
        zusammenfassung_mentorenseite = f"entferne Pause bei {', '.join([item.title(as_link=True, allow_interwiki=False) for item in mentoren_wieder_aktiv])};" + zusammenfassung_mentorenseite
        pass

    if mp_seite.text != str(wikicode):
        output.info(zusammenfassung_mentorenseite, "Mentorenaktivität")
        mp_seite.text = str(wikicode)
        print(mp_seite.text)
        mp_seite.save(summary=f"Bot: {zusammenfassung_mentorenseite}", minor=False, botflag=False)
        if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
            neue_verbleibende_bearbeitungen -= 1
    else:
        output.info("Wikipedia:Mentorenprogramm muss nicht aktualisert werden", "Mentorenaktivität")
    if com_seite.text != co_metoren_uebersichtsseite:
        com_seite.text = co_metoren_uebersichtsseite
        print(com_seite.text)
        com_seite.save(summary="Bot: aktualisiere Liste", minor=False, botflag=False)
        if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
            neue_verbleibende_bearbeitungen -= 1

    update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
    return
