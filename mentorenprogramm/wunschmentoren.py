import datetime
import re
import mwparserfromhell
import pywikibot
import pymysql
from werkzeuge import ueberwachung, output
from werkzeuge.catch_exceptions_scheduled import catch_exceptions
from werkzeuge.update_verbleibende_bearbeitungen import update_verbleibende_bearbeitungen

reaktionszeit_wunschmentor = datetime.timedelta(days=1)
reaktionszeit_mentor_ist_kein_mentor = datetime.timedelta(minutes=30)


def ist_wunschmentor_ein_mentor(wunschmentor, wikifam):
    """ Prüfen ob angegebener Benutzer wirklich ein Mentor ist und ob Benutzer auch aktiv ist
    :param wunschmentor: Userobjekt des Wunschmentors
    :return: bool
    """
    if wunschmentor.getUserPage("Vorlage Mentor").exists():
        return True
    elif pywikibot.Page(wikifam, f"Wikipedia:Mentorenprogramm/Mentorenvorlage/{wunschmentor.username}").exists():
        return True
    else:
        return False


def ist_mentor_aktiv(wunschmentor, vorlagen):
    """
    Prüfen ob Mentor aktuell aktiv ist, Co-Mentoren Vorschlagen
    :param wunschmentor: Userobjekt des Wunschmentors
    :param vorlagen: Vorlagen aller Mentoren
    :return: bool, [Co-Mentoren liste]
    """
    ist_aktiv = False
    co_mentoren = []
    for vorlage in vorlagen:
        if "Mentor" not in vorlage.name:  # Falsche vorlage
            continue
        if not wunschmentor.username.lower() in vorlage.get("Mentor").value.lower():  # Falscher Mentor
            continue
        ist_aktiv = False if "ja" in vorlage.get("Pause").value.lower() else True
        for param in vorlage.params:
            if "co-mentor" in param.lower():
                param = param.split("=")
                co_mentoren.append(param[1].strip())
        # co_mentoren.append(vorlage.get("Co-Mentor1").value)
        # output.info(vorlage.get("Co-Mentor3"), "Wunschmentoren")
        # exit()
    return ist_aktiv, co_mentoren


def ist_mentor_weiblich(mentor, mp_vorlagen):
    """ Prüfen ob angegebener Benutzer weiblich ist anhand der vorlage im BNR """
    for vorlage in mp_vorlagen:
        if "Mentor" not in vorlage.name:  # Falsche vorlage
            continue
        if not mentor.username.lower() in vorlage.get("Mentor").value.lower():  # Falscher Mentor
            continue
        try:
            return True if "ja" in vorlage.get("Weiblich").value else False
        except ValueError:
            return False
    return None


def mehrfacheinbindung_mentorengesucht_vorlagen_aufraeumen(mentor_gesucht_vorlagen, artikel_in_wm_gesucht, mp_vorlagen,
                                                           wikifam, wm_gesucht_wikitext):
    # "Mentor gesucht"-Vorlage wurde mehrfach eingebunden; Entferne Vorlagen ohne Wunschmentor
    output.info(f"Benutzer {artikel_in_wm_gesucht.title(with_ns=False)} hat mehrere Mentor gesucht "
                f"Vorlagen eingebunden", "Wunschmentoren")
    for vorlage in mentor_gesucht_vorlagen:  # alle vorlagen ohne Wunschmentoren entfernen
        if len(vorlage.params) < 1:
            wm_gesucht_wikitext.remove(vorlage)
    mentor_gesucht_vorlagen = wm_gesucht_wikitext.filter_templates(matches=".*Mentor gesucht.*")
    # "Mentor gesucht"-Vorlage wurde mehrfach eingebunden; Entferne Vorlagen mit inaktiven Mentoren (falls noch
    # mehr als eine Vorlage vorhanden ist)
    if len(mentor_gesucht_vorlagen) > 1:
        for vorlage in mentor_gesucht_vorlagen:
            if len(mentor_gesucht_vorlagen) == 1:
                break
            elif not ist_wunschmentor_ein_mentor(pywikibot.User(wikifam, vorlage.get(1).value)):
                wm_gesucht_wikitext.remove(mentor_gesucht_vorlagen[-1])
            elif not ist_mentor_aktiv(pywikibot.User(wikifam, vorlage.get(1).value), mp_vorlagen):
                wm_gesucht_wikitext.remove(mentor_gesucht_vorlagen[-1])
            mentor_gesucht_vorlagen = wm_gesucht_wikitext.filter_templates(matches=".*Mentor gesucht.*")
    # Falls noch mehr als eine "Mentor gesucht"-Vorlage vorhanden ist immer die letzt enfernen
    while len(mentor_gesucht_vorlagen) > 1:
        wm_gesucht_wikitext.remove(mentor_gesucht_vorlagen[-1])
        mentor_gesucht_vorlagen = wm_gesucht_wikitext.filter_templates(matches=".*Mentor gesucht.*")
    return wm_gesucht_wikitext, mentor_gesucht_vorlagen


@catch_exceptions(cancel_on_failure=True)
def wunschmentoren(wikifam, begrenzte_bearbeitungen, botuser, sql_verbindung, sql_tabellen, nonpublic):
    global wunschmentor_talk, wunschmentor, co_mentoren
    neue_verbleibende_bearbeitungen = begrenzte_bearbeitungen["verbleibende_bearbeitungen"]
    wunschmentor_gesucht_kategorie = pywikibot.Category(wikifam, title="Kategorie:Benutzer:Wunschmentor_gesucht")
    mp_seite = pywikibot.page.Page(wikifam, "Mentorenprogramm", ns=4)
    mp_vorlagen = mwparserfromhell.parse(mp_seite.text).filter_templates()

    # Hole die User die eine Info wünschen, wenn Mentee inaktiv wird
    optin_seite = mwparserfromhell.parse(
        pywikibot.Page(wikifam, "Mentorenprogramm/Projektorganisation/Opt-in-Liste", ns=4).text)
    optin_email = []
    for abschnitt in optin_seite.get_sections():
        ueberschrift = re.search("== (.*) ==", str(abschnitt))
        if ueberschrift:
            ueberschrift = ueberschrift[
                1]  # in 0 steht der komplette match in 1 der string der durch die klammern erfasst wurde
            if ueberschrift == "Opt-in-Liste: E-Mail-Benachrichtigung über Wunschmentorgesuche":
                benutzer = re.findall("# \[\[(.*)\]\]\n", str(abschnitt))
                if benutzer:
                    for benutzer in benutzer:
                        optin_email.append(benutzer.split("|")[1].lower().replace("&nbsp;", " "))

    output.info("Abarbeitung der Mentorengesuche", "Wunschmentoren")
    bearbeitete_wunschmentorgesuche = []
    for artikel_in_wm_gesucht in wunschmentor_gesucht_kategorie.articles():
        if not artikel_in_wm_gesucht.namespace().id in [2, 3]:
            output.info("Die Vorlage wurde im falschen Namespace hinterlegt", "Wunschmentoren")
            # ToDo: Korrekter namespace? Entfernung notwendig? vermutlich besser manuell
            continue
        mehrfach_vorlage = False
        vorlage_verschoben = False
        mentee_registriert = True
        mentee_info = False
        mentor_ist_aktiv = True
        mentor_info = False
        co_mentor_info = False
        mentor_ist_kein_mentor = False

        wm_gesucht_wikitext = mwparserfromhell.parse(artikel_in_wm_gesucht.text)

        gender_string_mentor_anrede = ""
        gender_string_mentor = ""
        gender_string_mentor_pronomen = ""

        mentor_gesucht_vorlagen = wm_gesucht_wikitext.filter_templates(matches=".*Mentor gesucht.*")

        if len(mentor_gesucht_vorlagen) > 1:
            mehrfach_vorlage = True
            wm_gesucht_wikitext, mentor_gesucht_vorlagen = mehrfacheinbindung_mentorengesucht_vorlagen_aufraeumen(
                mentor_gesucht_vorlagen, artikel_in_wm_gesucht, mp_vorlagen, wikifam, wm_gesucht_wikitext)
        artikel_in_wm_gesucht.text = str(wm_gesucht_wikitext)

        mentee_seite = artikel_in_wm_gesucht if not artikel_in_wm_gesucht.isTalkPage() else artikel_in_wm_gesucht.toggleTalkPage()
        mentee_seite_wikitext = mwparserfromhell.parse(mentee_seite.text)
        mentee_disk = artikel_in_wm_gesucht if artikel_in_wm_gesucht.isTalkPage() else artikel_in_wm_gesucht.toggleTalkPage()
        mentee_disk_wikitext = mwparserfromhell.parse(mentee_disk.text)
        mentee = pywikibot.User(wikifam, mentee_seite.title(with_ns=False))

        vorlage = mentor_gesucht_vorlagen[0]

        # Vorlage wurde auf Diskussionsseite hinterlassen. Vorlage verschieben
        if artikel_in_wm_gesucht.isTalkPage():
            vorlage_verschoben = True
            output.info(
                f"{artikel_in_wm_gesucht.title(with_ns=False)} hat das Gesuch auf seiner Diskussionsseite hinterlassen", "Wunschmentoren")
            mentee_seite_wikitext.insert(1, vorlage)
            mentee_disk_wikitext.remove(str(vorlage))

        # Vorlage wurde von einem User hinterlassen, der nicht registriert ist. Melden und entfernen
        if not mentee.isRegistered():
            mentee_registriert = False
            output.info(f"Benutzer {mentee.username} ist scheinbar nicht registriert", "Wunschmentoren")
            mentee_seite_wikitext.remove(str(vorlage))

        elif len(vorlage.params) < 1:  # keinen Wunschmentor angegeben
            output.info(
                f"{artikel_in_wm_gesucht.username} hat keinen Wunschmentor angegeben", "Wunschmentoren")  # Dürfte nie erreicht werden

        else:  # Wunschmentor angegeben
            wunschmentor = pywikibot.User(wikifam, vorlage.get(1).value)
            if wunschmentor.username.lower() in nonpublic["no_bnr"]["mentoraktivitaet"]:
                output.output(f"{wunschmentor.username.lower()} wünscht keine Aktualisierung", "Wunschmentoren")
                continue
            # output.info(wunschmentor.username, "Wunschmentoren")
            gender_string_mentor_anrede = "Deine" if ist_mentor_weiblich(wunschmentor, mp_vorlagen) else "Dein"
            gender_string_mentor_anrede_2 = "Deine" if ist_mentor_weiblich(wunschmentor, mp_vorlagen) else "Deinen"
            # gender_string_mentor_pronomen = "Sie" if ist_mentor_weiblich(wunschmentor, mp_vorlagen) else "Ihn"
            gender_string_mentor_pronomen = "Sie" if ist_mentor_weiblich(wunschmentor, mp_vorlagen) else "Er"
            gender_string_mentor = "Mentorin" if ist_mentor_weiblich(wunschmentor, mp_vorlagen) else "Mentor"

            if ist_wunschmentor_ein_mentor(wunschmentor, wikifam):  # Wunschmentor ist Mentor
                mentor_ist_aktiv, co_mentoren = ist_mentor_aktiv(wunschmentor, mp_vorlagen)

                bearbeitete_wunschmentorgesuche.append({"mentee": mentee.username, "wunschmentor": wunschmentor.username})

                if len(vorlage.params) >= 2:
                    if "ja" in vorlage.get(2).value.lower():  # Mentor wurde informiert.
                        output.info(f"{wunschmentor.username} wurde bereits informiert", "Wunschmentoren")

                        # Prüfen, wann der Wunschmentor informiert wurde
                        with sql_verbindung.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
                            query = (f"SELECT * FROM {sql_tabellen['wunschmentor']} WHERE mentee=%s AND wunschmentor=%s "
                                     "ORDER BY id asc;")
                            cursor.execute(query, (mentee.username, wunschmentor.username))
                            db_eintrag = cursor.fetchone()

                        if db_eintrag["anfrage"] < datetime.datetime.utcnow() - reaktionszeit_wunschmentor:
                            output.info(f"{mentee.username} wartet schon länger als "
                                        f"{reaktionszeit_wunschmentor.total_seconds()/60} Minuten.", "Wunschmentoren")
                            if db_eintrag["co_mentoren_informiert"]:
                                output.info(f"Die CO-Mentoren wurden bereits informiert", "Wunschmentoren")
                            else:
                                output.info(f"Informiere die CO-Mentoren", "Wunschmentoren")
                                co_mentor_info = True
                                with sql_verbindung.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
                                    query = (f"UPDATE {sql_tabellen['wunschmentor']} set co_mentoren_informiert=True "
                                             f"WHERE id={db_eintrag['id']};")
                                    cursor.execute(query)
                                    sql_verbindung.commit()

                    else:
                        output.info(f"Vorlage wurde verändert. An Parameter 2 steht ein unbekannter Wert", "Wunschmentoren")

                # Wunschmentor hat Pause
                elif not mentor_ist_aktiv:
                    output.info(f"{wunschmentor.username} hat Pause.", "Wunschmentoren")
                    mentee_seite_wikitext.replace(str(vorlage), "{{Mentor gesucht}}")

                # Mentor informieren
                else:
                    output.info(f"{mentee.username} hat {wunschmentor.username} als "
                                f"Wunsch{gender_string_mentor.lower()} angegeben", "Wunschmentoren")
                    mentee_seite_wikitext.replace(str(vorlage),
                                                  "{{" + str(vorlage.name) + "|" + str(vorlage.get(1).value) + "|ja}}")
                    mentor_info = True
                    wunschmentor_talk = wunschmentor.getUserTalkPage()

            else:  # Wunschmentor ist kein Mentor
                output.info(
                    f"{artikel_in_wm_gesucht.username} hat einen Mentor angegeben, der kein Mentor ist.", "Wunschmentoren")

                if artikel_in_wm_gesucht.latest_revision.timestamp + reaktionszeit_mentor_ist_kein_mentor < datetime.datetime.utcnow():
                    output.info(
                        f"Informiere {artikel_in_wm_gesucht.username} darüber und passe die Vorlage an", "Wunschmentoren")
                    mentor_ist_kein_mentor = True
                    mentee_seite_wikitext.replace(str(vorlage), "{{Mentor gesucht}}")

                else:
                    output.info(f"Warte noch etwas, damit {artikel_in_wm_gesucht.username} Zeit hat sich einen "
                                f"anderen Mentoren  auszusuchen", "Wunschmentoren")

        if mentor_info:
            wunschmentor_nachricht = (
                f"\n\n== {mentee_seite.title(with_ns=False)} hat sich dich als {gender_string_mentor} gewünscht ==\n"
                f"Hallo {wunschmentor.username},\n\n"
                f"[[{mentee_seite.title()}]] hat sich dich als {gender_string_mentor} gewünscht.\n\n")

            if mehrfach_vorlage:
                wunschmentor_nachricht += ("Die \"Mentor gesucht\"-Vorlage wurde mehrfach eingebunden. Ich habe alle "
                                           "Einbindungen bis auf eine entfernt. Bitte prüfe,ob du auch wirklich "
                                           "gemeint bist.\n\n")

            if vorlage_verschoben:
                wunschmentor_nachricht += ("Außerdem habe ich die Vorlageneinbindung von der Diskussionsseite auf die "
                                           "Benutzerseite verschieben müssen.\n\n")

            wunschmentor_nachricht += "Grüße ~~~~\n"

            wunschmentor_talk.save(summary="Bot: Information über Wunschmentorengesuch",
                                   appendtext=wunschmentor_nachricht, minor=False, botflag=False)

            if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                neue_verbleibende_bearbeitungen -= 1
            with sql_verbindung.cursor(pymysql.cursors.DictCursor) as cursor:
                query = (f"INSERT INTO `{sql_tabellen['wunschmentor']}` (`mentee`, `wunschmentor`, `anfrage`, "
                         "`co_mentoren_informiert`) VALUES (%s, %s, %s, False)")
                cursor.execute(query, (mentee.username, wunschmentor.username, datetime.datetime.utcnow()))
                sql_verbindung.commit()

            if wunschmentor.username in optin_email and wunschmentor.isEmailable():
                wunschmentor.send_email(subject=f"{mentee_seite.title(with_ns=False)} hat sich dich als {gender_string_mentor} gewünscht",
                                        text=f"Hallo {wunschmentor.username},\n\n"
                                             f"{mentee_seite.title()} hat sich dich als {gender_string_mentor} gewünscht.\n\n"
                                             f"Viele Grüße\nWosretbot", ccme=True)

        zusammenfassung_mentee_disk = "Bot:"
        nachricht_mentee = "\n\n== Dein Mentorengesuch ==\n\n"

        if mentor_info:
            zusammenfassung_mentee_disk += f" {gender_string_mentor_anrede.lower()} Wunsch{gender_string_mentor.lower()} wurde informiert;"
            nachricht_mentee += (
                f"ich habe {gender_string_mentor_anrede_2.lower()} Wunsch{gender_string_mentor.lower()} über "
                f"dein Gesuch informiert. {gender_string_mentor_pronomen} wird sich sicherlich bald bei dir melden.\n\n")
            mentee_info = True

        if vorlage_verschoben:
            zusammenfassung_mentee_disk += " verschiebung der Vorlage;"
            nachricht_mentee += ("Du hast die Vorlage versehentlich auf deiner Diskussionsseite eingebunden. Ich habe "
                                 "sie für dich auf deine Benutzerseite verschoben \n\n")
            mentee_info = True

        if not mentor_ist_aktiv and "ja" not in vorlage.get(2, default="").value.lower():
            zusammenfassung_mentee_disk += " dein Wunschmentor ist aktuell nicht aktiv;"
            nachricht_mentee += (
                f"leider ist {gender_string_mentor_anrede.lower()} Wunsch{gender_string_mentor.lower()} akutell"
                f" leider nicht aktiv. Such dir gerne [[WP:MP|hier]] einen anderen Mentoren aus. Ich "
                f"habe die Vorlage solange in ein allgemeines Mentorengesuch umgewandelt.\n\n")
            mentee_info = True

        if mentor_ist_kein_mentor:
            zusammenfassung_mentee_disk += " dein Wunschmentor ist kein Mentor"
            nachricht_mentee += (f"leider ist {gender_string_mentor_anrede.lower()} "
                                 f"Wunsch{gender_string_mentor.lower()} kein Mentor. Such dir gerne "
                                 f"[[WP:MP|hier]] einen anderen Mentoren aus. Ich habe die Vorlage solange in ein "
                                 f"allgemeines Mentorengesuch umgewandelt.\n\n")
            mentee_info = True
        nachricht_mentee += ("Bitte beachte: Ich bin nur ein [[WP:Bots|Computerprogramm]]. Wenn du mir antworten "
                             f"möchtest, oder Fragen zu dieser Mitteilung hast, klicke bitte [[BD:{botuser.username}|hier]] und "
                             "hinterlass eine Nachricht.\n\n"
                             "Grüsse ~~~~")

        if not mentee_registriert:
            nachricht_mentee = "{{ers:Vorlage:MentorNoIP}}"
            zusammenfassung_mentee_disk += " das Mentorenprogramm steht nur registrierten Nutzern zur Verfügung;"
            mentee_info = True

        if mentee_info:
            zusammenfassung_mentee_seite = "Bot:"
            if mentor_info:
                zusammenfassung_mentee_seite += " Mentor wurde informiert;"
            if vorlage_verschoben:
                zusammenfassung_mentee_seite += " Vorlage von Disk hier her verschoben;"
            if not mentee_registriert:
                zusammenfassung_mentee_seite += ("Vorlage entfernt; Das Mentorenprogramm steht nur angemeldeten "
                                                 "Benutzern zur verfügung")
            if mehrfach_vorlage:
                zusammenfassung_mentee_seite += " mehrfacheinbindungen entfernt;"

            mentee_seite.text = str(mentee_seite_wikitext)
            mentee_seite.save(summary=zusammenfassung_mentee_seite, minor=False)
            if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                neue_verbleibende_bearbeitungen -= 1

            mentee_disk.text = str(mentee_disk_wikitext).strip()
            mentee_disk.text += nachricht_mentee
            mentee_disk.save(summary=zusammenfassung_mentee_disk, minor=False, botflag=False)
            if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                neue_verbleibende_bearbeitungen -= 1

        if co_mentor_info:
            nachricht_co_mentor = ("\n\n== Wunschmentorgesuch ==\n"
                                   "Hallo {},\n\n"
                                   # f"du bist als Co-Mentor für {wunschmentor.username} hinterlegt. "
                                   f"{wunschmentor.username} hat noch nicht auf das Gesuch von [[{mentee.title()}]] "
                                   "reagiert.\n\n"
                                   "Schau mal ob du als Co-Mentor was machen kannst.\n\n"
                                   "Grüße ~~~~")
            for co_mentor in co_mentoren:
                co_mentor_user = pywikibot.User(wikifam, co_mentor)
                co_mentor_disk = co_mentor_user.getUserTalkPage()
                co_mentor_disk.save(
                    summary=f"Bot: {mentee.username} wünscht sich {wunschmentor.username} als {gender_string_mentor}",
                    appendtext=nachricht_co_mentor.format(co_mentor_user.username),
                    minor=False, botflag=False)
                if begrenzte_bearbeitungen["bearbeitungen_begrenzen"]:
                    neue_verbleibende_bearbeitungen -= 1

        # Nach jedem durchlauf prüfen ob Abbruch erforderlich
        if ueberwachung.bedingungen_ueberwachen(neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen, botuser,
                                                wikifam):
            break

    # gelöschte Wunschmentorgesuche aufräumen
    with sql_verbindung.cursor(cursor=pymysql.cursors.DictCursor) as cursor:
        # aktuellen Datenbestand holen
        query = f"select * from {sql_tabellen['wunschmentor']} ORDER BY id asc;"
        cursor.execute(query)
        db_eintrag = cursor.fetchall()

        # Gültige Einträge ausfiltern
        for e in db_eintrag:
            if {"mentee": e["mentee"], "wunschmentor": e["wunschmentor"]} in bearbeitete_wunschmentorgesuche:
                db_eintrag.remove(e)
                bearbeitete_wunschmentorgesuche.remove({"mentee": e["mentee"], "wunschmentor": e["wunschmentor"]})
            if len(bearbeitete_wunschmentorgesuche) == 0:
                break

        # Ungültige Einträge aus Datenbenstand entfernen
        query = f"DELETE FROM {sql_tabellen['wunschmentor']} WHERE id=%s"
        data = list([(x["id"],) for x in db_eintrag])
        cursor.executemany(query, data)
        sql_verbindung.commit()

    update_verbleibende_bearbeitungen(wikifam, botuser, neue_verbleibende_bearbeitungen, begrenzte_bearbeitungen)
    return


if __name__ == "__Main__":
    pass
