import argparse
import configparser
import tomllib
import sys
import time

import pymysql
import pywikibot
from pytz import timezone
from pywikibot.data import mysql
import schedule

from diverses.gazette import gazette
from diverses.tagesueberschriften import tagesueberschriften
from mentorenprogramm.mentorenaktivitaet import mentorenaktivitaet
from mentorenprogramm.menteeaktivitaet import menteeaktivitaet
from mentorenprogramm.wunschmentoren import wunschmentoren
from werkzeuge import ueberwachung, output
from werkzeuge.catch_exceptions_scheduled import catch_exceptions


if __name__ == "__main__":
    # Konfiguarationen einlesen
    config_replicadb = configparser.ConfigParser()
    config_replicadb.read(".my.cnf")
    config = configparser.ConfigParser()
    config.read("conf.ini")
    with open("nonpublic.toml", "rb") as nonpublic_toml:
        nonpublic = tomllib.load(nonpublic_toml)

    # Starparameter configurieren
    parser_aufrufargumente = argparse.ArgumentParser(description='Parameter für den Wosretbot.')
    parser_aufrufargumente.add_argument("--debug", default=False,
                                        help="Aktiviert den Debugmodus",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--local", default=False,
                                        help="Aktiviert den Lokalmodus",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--wunschmentoren", default=False,
                                        help="Abarbeitung der Wunschmentorengesuche",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--mentorenaktivitaet", default=False,
                                        help="Aktualiseren der Aktivitätsstati der Mentoren",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--menteeaktivitaet", default=False,
                                        help="Überprüfen der Aktivitätsstati der Mentees",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--tagesueberschriften", default=False,
                                        help="Aktualisieren der Tagesüberschriften",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--gazette", default=False,
                                        help="Versand der Gazette",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--simulate", default=False,
                                        help="Änderungen nicht speichern, nur simulieren",
                                        action=argparse.BooleanOptionalAction)
    parser_aufrufargumente.add_argument("--cron", default=False,
                                        help="Conjob aktivieren",
                                        action=argparse.BooleanOptionalAction)
    aufrufargumente = parser_aufrufargumente.parse_args()

    # Abbrechen, falls keine Parameter angegeben worden sind
    if len(sys.argv) < 2:
        parser_aufrufargumente.print_help()
        sys.exit("Keine Parameter angegeben")

    if aufrufargumente.simulate:
        pywikibot.handle_args(['-simulate'])

    # Login
    if aufrufargumente.local:
        wikifam = pywikibot.Site('de', 'owntestwiki')
    else:
        wikifam = pywikibot.Site('de', 'wikipedia')
    wikifam.login()

    # Eigenen Benutzernamen auslesen für Notaus etc.
    botuser = pywikibot.User(wikifam, wikifam.user())

    # verbleibender Edits; Aktivierung verbleibender Edits
    verbleibende_bearbeitungen_page = pywikibot.page.Page(
        wikifam, f"Benutzer:{botuser.title(with_ns=False)}/Verbleibende_Bearbeitungen")
    verbleibende_bearbeitungen = int(verbleibende_bearbeitungen_page.text)
    neue_verbleibende_bearbeitungen = verbleibende_bearbeitungen  # Schutz von zugriff bevor zugewiesen wurde
    if not aufrufargumente.debug:
        bearbeitungen_begrenzen = False  # Abgeschaltet
    else:
        bearbeitungen_begrenzen = False

    # Prüfen, ob eine abbruch bedingung vorliegt
    if ueberwachung.bedingungen_ueberwachen(verbleibende_bearbeitungen, bearbeitungen_begrenzen, botuser, wikifam):
        if not aufrufargumente.debug:
            exit()
        pass

    # MYSQL Datenbank
    if aufrufargumente.debug:
        sql = {"server": config["datenbank.debug"]["server"],
               "benutzer": config_replicadb["client"]["user"],
               "passwort": config_replicadb["client"]["password"],
               "db": config["datenbank.debug"]["db_wunschmentoren"]}
        sql_tabellen = {
            "wunschmentor": config["datenbank.debug"]["tabelle_wunschmentor"],
            "menteeaktivitaet": config["datenbank.debug"]["tabelle_menteeaktivitaet"],
            "mentorenaktivitaet": config["datenbank.debug"]["tabelle_mentorenaktivitaet"],
        }
    else:
        sql = {"server": config["datenbank.produktiv"]["server"],
               "benutzer": config_replicadb["client"]["user"],
               "passwort": config_replicadb["client"]["password"],
               "db": config["datenbank.produktiv"]["db_wunschmentoren"]}
        sql_tabellen = {
            "wunschmentor": config["datenbank.produktiv"]["tabelle_wunschmentor"],
            "menteeaktivitaet": config["datenbank.produktiv"]["tabelle_menteeaktivitaet"],
            "mentorenaktivitaet": config["datenbank.produktiv"]["tabelle_mentorenaktivitaet"],
        }
    sql_verbindung = pymysql.connect(
        host=sql["server"],
        user=sql["benutzer"],
        password=sql["passwort"],
        database=sql["db"])

    # Starte schedule:
    if aufrufargumente.cron:
        schedule.every(5).minutes.do(wunschmentoren, wikifam=wikifam,
                                     begrenzte_bearbeitungen={"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                              "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                     botuser=botuser,
                                     sql_verbindung=sql_verbindung,
                                     sql_tabellen=sql_tabellen,
                                     nonpublic=nonpublic,
                                     )
        schedule.every(1).day.at("00:00:20", timezone("Europe/Amsterdam")).do(tagesueberschriften,
                                                                          wikifam=wikifam,
                                                                          botuser=botuser,
                                                                          begrenzte_bearbeitungen={
                                                                              "bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                                              "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                                                          )
        # schedule.every().day.at("00:05:00", timezone("Europe/Amsterdam")).do(mentorenaktivitaet,
        #                                                                   wikifam=wikifam,
        #                                                                   begrenzte_bearbeitungen={
        #                                                                       "bearbeitungen_begrenzen": bearbeitungen_begrenzen,
        #                                                                       "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
        #                                                                   botuser=botuser,
        #                                                                   sql_verbindung=sql_verbindung,
        #                                                                   sql_tabellen=sql_tabellen,
        #                                                                   nonpublic=nonpublic,
        #                                                                   )
        # schedule.every().friday.at("00:10:00", timezone("Europe/Amsterdam")).do(menteeaktivitaet,
        #                                                                      wikifam=wikifam,
        #                                                                      begrenzte_bearbeitungen={
        #                                                                          "bearbeitungen_begrenzen": bearbeitungen_begrenzen,
        #                                                                          "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
        #                                                                      botuser=botuser,
        #                                                                      sql_verbindung=sql_verbindung,
        #                                                                      sql_tabellen=sql_tabellen,
        #                                                                      nonpublic=nonpublic,
        #                                                                      )
        # schedule.every().monday.at("00:10:00", timezone("Europe/Amsterdam")).do(gazette,
        #                                                                      wikifam=wikifam,
        #                                                                      botuser=botuser,
        #                                                                      begrenzte_bearbeitungen={
        #                                                                          "bearbeitungen_begrenzen": bearbeitungen_begrenzen,
        #                                                                          "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
        #                                                                      nonpublic=nonpublic,
        #                                                                      )

    # Verarbeitung der Mentorenwünsche ausführen
    if aufrufargumente.wunschmentoren:
        neue_verbleibende_bearbeitungen = wunschmentoren(wikifam,
                                                         {"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                          "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                                         botuser, sql_verbindung, sql_tabellen, nonpublic)

    # Verarbeitung der Mentorenaktivität
    if aufrufargumente.mentorenaktivitaet:
        neue_verbleibende_bearbeitungen = mentorenaktivitaet(wikifam,
                                                             {"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                              "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                                             sql_verbindung, sql_tabellen, botuser, nonpublic)

    # Verarbeitung der Menteeaktivität
    if aufrufargumente.menteeaktivitaet:
        neue_verbleibende_bearbeitungen = menteeaktivitaet(wikifam,
                                                           {"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                            "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                                           sql_verbindung, sql_tabellen, botuser, nonpublic)

    # Verarbeitung der tagesüberschriften
    if aufrufargumente.tagesueberschriften:
        neue_verbleibende_bearbeitungen = tagesueberschriften(wikifam,
                                                              {"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                               "verbleibende_bearbeitungen": verbleibende_bearbeitungen},
                                                              botuser)

    # Verarbeitung der tagesüberschriften
    if aufrufargumente.gazette:
        neue_verbleibende_bearbeitungen = gazette(wikifam,
                                                  botuser,
                                                  {"bearbeitungen_begrenzen": bearbeitungen_begrenzen,
                                                   "verbleibende_bearbeitungen": verbleibende_bearbeitungen}, nonpublic)


    # Führe Schedules aus
    if aufrufargumente.cron:
        while True:
            schedule.run_pending()
            time.sleep(1)
